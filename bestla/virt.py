#! /usr/bin/python
#
# Copyright 2015 John H. Dulaney <jdulaney@fedoraproject.org>
#
# Licensed under the GNU General Public License Version 2
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# bestla -- Demonstrate easily spinning up virtual machines using web ui
#
# virt.py  Objects for handling virtual machines

import os
import threading
import time
from config import image_location, tree, iface, subnet
from ip import get_ip
from tempfile import mkdtemp
import shutil
import subprocess
from kickstart import kickstart_c7
from bestla import vm_list

exitFlag = 0


def set_status(name, status):
    """Pop vm from list, set status, return to list"""
    global vm_list
    for vm in vm_list:
        if vm.name == name:
            vm_index = vm_list.index(vm)
    vm = vm_list.pop(vm_index)
    vm.status = status
    vm_list.append(vm)
    return 0

def get_libvirt_status(name):
    virt_states = subprocess.check_output(['virsh', 'list', '--all'])
    states = virt_states.split('\n')
    for line in states:
        if name in line:
            items = filter(None,line.split(' '))
            if len(items) == 4:
                return items[2] + ' ' + items[3]
            else:
                return items[2]
    return 0

def vm_command(name, command):
    if command == 'Delete':
        vm_delete = DeleteVM(name)
        vm_delete.start()
    else:
        vm_action = VMWork(name, command)
    return 0

def pxe_install(name, num_cpus, ram, tree, disk, ip):
    ks_dir = mkdtemp()
 
    ks = ks_dir + '/anaconda-ks.cfg'
    ks_out = kickstart_c7(ip, subnet)
    ks_f = open(ks, 'w')
    ks_f.write(ks_out)
    ks_f.close()

    os.system('virt-install' +
            ' --connect' +
            ' qemu:///session' +
            ' --nographics' +
            ' --name=' + name +
            ' -r=' + str(ram) +
            ' --vcpus=' + str(num_cpus) +
            ' -w ' + 'bridge=' + iface +
            ' --disk' + ' path=' + disk + ',size=10' +
            ' -l=' + tree +
            ' --initrd-inject=' + ks +
            ' --extra-args="ks=file:/anaconda-ks.cfg ksdevice=link console=ttyS0"' +
            ' --channel=none' +
            ' --noautoconsole')
    shutil.rmtree(os.path.dirname(ks))
    return 0


class InstallThread(threading.Thread):
    def __init__(self, name, num_cpus, ram, tree, ip, disk, image=None, method='pxe'):
        threading.Thread.__init__(self)
        self.name = name
        self.num_cpus = num_cpus
        self.ram = ram
        self.tree = tree
        self.disk = disk
        self.image = image
        self.method = method
        self.ip = ip
    def run(self):
        if self.method == 'pxe':
            pxe_install(self.name, self.num_cpus, self.ram, self.tree, self.disk, self.ip)
        # Wait until vm completes installation
        truth = True
        while truth == True:
            time.sleep(10)
            status = get_libvirt_status(self.name)
            if 'shut off' == status:
                truth = False
                break
            else:
                if 'crashed' == status:
                    print('uh, oh') # Need to do better
                    break
        subprocess.call(['virsh', 'start', self.name])
        set_status(self.name, 'running')
        return 0

class VMStatusPoll(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
    def run(self):
        global vm_list
        while True:
            for vm in vm_list:
                status = get_libvirt_status(vm.name)
                if vm.status != 'Installing':
                    set_status(vm.name, status)
            time.sleep(30)
        return 0

class VMWork(threading.Thread):
    def __init__(self, name, command):
        threading.Thread.__init__(self)
        self.name = name
        self.command = command
    def run(self):
        global vm_list
        if self.command == 'Start':
            action = 'start'
            success = 'started'
            new_status = 'running'
        elif self.command == 'Stop':
            action = 'destroy'
            success = 'destroyed'
            new_status = 'shut off'
        elif self.command == 'Pause':
            action = 'suspend'
            success = 'suspended'
            new_status = 'paused'
        elif self.command == 'Resume':
            action = 'resume'
            success = 'resumeded'
            new_status = 'running'
        is_good = subprocess.check_output(['virsh', action, self.name])
        if success in is_good:
            set_status(self.name, new_status)
        return 0

class DeleteVM(threading.Thread):
    def __init__(self, name):
        threading.Thread.__init__(self)
        self.name = name
    def run(self):
        global vm_list
        is_good = subprocess.check_output(['virsh', 'destroy', self.name])
        if 'destroyed' in is_good:
            set_status(self.name, 'deleting')
            is_good = subprocess.check_output(['virsh', 'undefine', self.name])
            for vm in vm_list:
                if vm.name == self.name:
                    os.remove(vm.disk)
                    vm_index = vm_list.index(vm)
                    vm_list.pop(vm_index)
        return 0


class VirtualMachine():
    def __init__(self, name, master_image=False):
        self.name = name
        self.user = ''
        self.status = 'Uninitialized'
        self.disk = ''
        self.ram = ''
        self.num_cpus = ''
        self.tree = tree
        self.ip = ''
        self.creation_time = ''
    def install(self, ssh_key):
        """Install new virtual machine"""
        self.status = 'Installing'
        self.disk = image_location + self.name + '.img'
        self.ip = get_ip()

        # Temporary defaults:
        self.ram = '1024'
        self.num_cpus = '1'

        vm_install = InstallThread(self.name, self.num_cpus,
                self.ram, self.tree, self.ip, self.disk)
        vm_install.start()
        return 0


status_poll = VMStatusPoll()
status_poll.daemon = True
