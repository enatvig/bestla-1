#! /usr/bin/python
import trace
from bestla.virt import status_poll
from bestla import webui

if __name__ == '__main__':
    print('bestla')
    status_poll.start()
    webui.run(debug=True)
